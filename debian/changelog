golang-github-pierrec-lz4 (4.1.18-1) unstable; urgency=medium

  * New upstream version 4.1.18 (Closes: #1000257)
  * Update section to golang
  * Update Standards-Version to 4.6.2 (no changes)
  * Drop 0001-fix-source-file-permission.patch
  * Install extra testdata
  * Add patch to skip broken TestWriterLegacy
  * Add packer << 1.6.6+ds2-3~ to Breaks

 -- Shengjing Zhu <zhsj@debian.org>  Sun, 30 Jul 2023 09:28:29 +0800

golang-github-pierrec-lz4 (2.5.2-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.1, no changes needed.
  * Apply multi-arch hints. + golang-github-pierrec-lz4-dev: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 23 Nov 2022 21:59:33 +0000

golang-github-pierrec-lz4 (2.5.2-1) unstable; urgency=medium

  [ Alexandre Viau ]
  * Point Vcs-* urls to salsa.debian.org.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Change priority extra to priority optional.

  [ Shengjing Zhu ]
  * New upstream version 2.5.2
  * Fix uscan watch file to track upstream release
  * Update maintainer and uploader email address
  * Update Standards-Version to 4.5.0
  * Add Rules-Requires-Root
  * Stop building cmd lz4c
  * Drop golang-github-pierrec-xxhash-dev, not needed
  * Bump debhelper-compat to 13
  * Add patch to fix source file permission
  * Only track v2.x release
  * Add golang-github-frankban-quicktest-dev to Build-Depends

 -- Shengjing Zhu <zhsj@debian.org>  Sun, 21 Jun 2020 22:50:46 +0800

golang-github-pierrec-lz4 (0.0~git20170519.0.5a3d224-2) unstable; urgency=medium

  [ Paul Tagliamonte ]
  * Remove Built-Using from arch:all -dev package

  [ Shengjing Zhu ]
  * Miss golang-github-pierrec-xxhash-dev depends on -dev

 -- Shengjing Zhu <i@zhsj.me>  Fri, 11 Aug 2017 16:36:29 +0800

golang-github-pierrec-lz4 (0.0~git20170519.0.5a3d224-1) unstable; urgency=medium

  * Adopt package (Closes: #865336).
  * New snapshot release.
  * d/compat: bump to v10.
  * d/control:
    + Bump Standards-Version to 4.0.0.
    + Bump dependency of debhelper to 10.
    + Add autopkgtest-pkg-go Testsuite.
  * d/copyright:
    + Change format url to https.
    + Add myself to copyright holder.

 -- Shengjing Zhu <i@zhsj.me>  Fri, 28 Jul 2017 01:54:44 +0800

golang-github-pierrec-lz4 (0.0~git20151216.222ab1f-2) unstable; urgency=medium

  * deb/control:
    + update my Uploaders email address.
    + bump standards to 3.9.8 (no changes needed).
    + Use a secure transport for the Vcs-Git and Vcs-Browser URL [Paul
      Tagliamonte]
  * deb/copyright:
    + update my email address, expand copyright span.
  * deb/rules: add override for dh_auto_test (expand timeout, Closes: #844343).
  * add deb/watch (uses Eriberto's pseudopackage for "No-Release").

 -- Daniel Stender <stender@debian.org>  Mon, 05 Dec 2016 14:29:09 +0100

golang-github-pierrec-lz4 (0.0~git20151216.222ab1f-1) unstable; urgency=medium

  * Initial release (Closes: #807912).

 -- Daniel Stender <debian@danielstender.com>  Fri, 25 Dec 2015 22:16:39 +0100
