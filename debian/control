Source: golang-github-pierrec-lz4
Section: golang
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Shengjing Zhu <zhsj@debian.org>,
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-frankban-quicktest-dev,
Standards-Version: 4.6.2
Homepage: https://github.com/pierrec/lz4
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-pierrec-lz4
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-pierrec-lz4.git
XS-Go-Import-Path: github.com/pierrec/lz4
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-go

Package: golang-github-pierrec-lz4-dev
Architecture: all
Depends: ${misc:Depends},
         ${shlibs:Depends},
Breaks: packer (<< 1.6.6+ds2-3~),
Multi-Arch: foreign
Description: LZ4 compression and decompression in pure Go
 This library implements reading and writing lz4 compressed data (a frame),
 as specified in
 http://fastcompression.blogspot.fr/2013/04/lz4-streaming-format-final.html,
 using an io.Reader (decompression) and io.Writer (compression).  It is
 designed to minimize memory usage while maximizing throughput by being
 able to [de]compress data concurrently.
